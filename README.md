# Buchstabenuhr 8x16

Uhrzeit als Schrift im schlichten und schicken 8x16 Format.
![Buchstabenuhr_Bild](/doc/front.jpg)

## Hardware

### Benötigte Teile

- Arduino Nano v3
- [Adafruit 8x16 LED matrix](https://www.adafruit.com/product/2043 "Adafruit 8x16 LED matrix")
- [DS1307 Real Time Clock](https://www.adafruit.com/product/3296 "DS1307 Real Time Clock")
- USB-Netzteil mit Mini-B Stecker
- Acrylglasplatten für Lasercutte
- 2-Komponentenkleber

### Verkabelung

Die Verkabelung ist relativ simpel. Da die sowohl die Real Time Clock (RTC), als auch die 8x16 LED-Matrix das I2c Protokoll verwenden werden nur vier Ausgänge des Arduinos benötigt. Die beiden Analogen Ausgänge (A4 und A5) dienen als SDA und SCL Eingang für die beiden Module. Die GND und 5V Pins versorgen die beiden Module mit Spannung. Folgende Abbildung zeigt die Verkabelung:

![Verkabelung des Arduino](/hardware/HardwareLayout_Steckplatine.png "Hardware Layout")

Damit die Uhr mit Strom versorgt wird ist ein einfaches USB-Netzteil nötig. Der USB-Port des Arduinos wird sowohl zum stellen der Uhrzeit (siehe Software), als auch zur dauerhaften Stromversorgung der Uhr genutzt.

### Gehäuse

Das Gehäuse besteht aus mehreren Einzelteilen die aus einer Acrylglasplatte ausgelasert wurden. Die SVG Vorlage befindet sich im [Hardware Ordner](/hardware/housing/). Falls Sie keinen Laserdrucker haben, können Sie auch einen Onlineservice wie zum Beispiel [Formulor](http://www.formulor.de "Formulor") nutzen, um die Teile zu erstellen.

Für das Zusammenbauen der Teile wird ein 2-Komponentenkleber benötigt. Die Rückplatte sollte besser nur lose befestigt werden (Beispielsweise mit doppelseitigem Klebeband) damit man später noch an die verbaute Hardware rankommt.

## Software

Die Software nutzt die [Arduino IDE](https://www.arduino.cc/en/main/software "Arduino IDE ") und wurde mit der Version 1.6.13 getestet. Folgende externe Bibliotheken sind für die Uhr erforderlich und müssen installiert werden (siehe [link](https://www.heise.de/make/artikel/Arduino-Bibliotheken-einbinden-und-verwalten-3643786.html "link")). 
Alle Bibliotheken befinden sich auch im [libs-Ordner](/libs/). 

- [Adafruit LED Backpack](https://github.com/adafruit/Adafruit_LED_Backpack)
- [Adafruit-GFX-Library](https://github.com/adafruit/Adafruit-GFX-Library)
- [DS1307RTC](https://www.pjrc.com/teensy/td_libs_DS1307RTC.html)
- [RTClib](https://github.com/adafruit/RTClib)

Die drei Programme im [arduino-Ordner](/arduino/) werden im Folgenden erklärt:

### Matrix Test

Führen Sie das *MatrixTest* Programm als erstes auf ihrem Arduino aus und testen Sie, ob das angeschlossene Matrix-Panel richtig funktioniert. Es sollte leuchten und verschiedene Muster zeigen. Falls das nicht der Fall sein sollte, dann überprüfen Sie bitte die Verkabelung erneut und schauen Sie, dass die Matrix mit ausreichend Strom versorgt wird.

### Zeit einstellen

Das Programm *SetRTCTime* dient dazu die auf dem RTC-Chip gespeicherte Zeit einzustellen. Dabei wird der aktuelle Zeitstempel des verbundenen PCs genutzt. Beachten Sie auch, dass die Uhr nicht automatisch zwischen Sommer- und Winterzeit umschaltet. Sie müssen für das Umstellen den Code zur Zeitumstellung erneut ausführen. Da das RTC-Modul mit einer Knopfzellenbatterie mit Strom versorgt wird bleibt die eingestellte Zeit korrekt. Zumindest für eine gewisse Zeit. Auch ein gutes RTC-Modul hat eine gewisse Ungenauigkeit und muss alle paar Jahre bzw. Monate neu eingestellt werden.

### Buchstabenuhr
Der eigentliche Code der Uhr ist im Ordner *Buchstabenuhr*. Nachdem Sie die Uhrzeit erfolgreich eingestellt haben können Sie den Code einfach kompilieren und auf den Arduino übertragen. Die Uhr sollte dann nach kurzer Zeit die richtige Uhrzeit als Text anzeigen. 

Falls Sie vorhaben das Layout der Uhr zu ändern und so beispielsweise eine andere Sprache zu unterstützen, dann müssen Sie nur eine Anpassung an der [WordClockLayout.h](/arduino/Buchstabenuhr/WordClockLayout.h) Datei vornehmen. Dort werden alle benötigten Wörter und die dazugehörige Displayfläche definiert.
