#if !defined(WORDCLOCKLAYOUT_H)
#define WORDCLOCKLAYOUT_H

typedef struct {
  uint8_t row;
  uint8_t col;
  uint8_t width;
} ClockWord;

ClockWord cw_Es  = {0, 0, 2};
ClockWord cw_Ist  = {0, 3, 3};
ClockWord cw_fuenf = {0, 7, 4};
ClockWord cw_zehn  = {0, 11, 4};
ClockWord cw_viertel  = {1, 0, 7};
ClockWord cw_zwanzig  = {1, 7, 7};
ClockWord cw_halb  = {2, 0, 4};
ClockWord cw_dreiviertel  = {2, 4, 11};
ClockWord cw_nach  = {3, 0, 4};
ClockWord cw_vor = {3, 4, 3};
ClockWord cw_halb_h  = {3, 8, 4};
ClockWord cw_Ein  = {3, 13, 3};
ClockWord cw_Eins  = {4, 0, 4};
ClockWord cw_Zwei  = {4, 4, 4};
ClockWord cw_Drei  = {4, 8, 4};
ClockWord cw_Vier  = {4, 12, 4};
ClockWord cw_Fuenf  = {5, 0, 4};
ClockWord cw_Sechs  = {5, 4, 5};
ClockWord cw_Sieben  = {5, 9, 6};
ClockWord cw_Acht  = {6, 0, 4};
ClockWord cw_Neun  = {6, 4, 4};
ClockWord cw_Zehn  = {6, 8, 4};
ClockWord cw_Elf  = {6, 12, 3};
ClockWord cw_Zwoelf  = {7, 0, 5};
ClockWord cw_Uhr  = {7, 6, 3};
ClockWord cw_Nacht  = {7, 10, 5};

ClockWord cwArr_Hour[12] {cw_Eins,cw_Zwei,cw_Drei,cw_Vier,cw_Fuenf,cw_Sechs,cw_Sieben,cw_Acht,cw_Neun,cw_Zehn,cw_Elf,cw_Zwoelf};


#endif
