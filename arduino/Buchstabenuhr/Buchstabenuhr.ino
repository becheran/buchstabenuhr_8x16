#include <Wire.h>
#include "Adafruit_LEDBackpack.h"
#include "Adafruit_GFX.h"
#include <TimeLib.h>
#include <DS1307RTC.h>
#include "WordClockLayout.h"

#define DEBUG 1 // Uncomment to disable console debugging

Adafruit_8x16matrix matrix = Adafruit_8x16matrix();

uint8_t timeStatusCode = 0; // = minutes/5 -> Triggers different updates
uint8_t oldTimeCode = 99;

void setup() {
  matrix.begin(0x70);  // pass in the address
  matrix.setBrightness(1); // Value from 0-15
  setSyncProvider(RTC.get);   // the function to get the time from the RTC
#ifdef DEBUG
  printInitStatus();
#endif
}

void loop() {
  if (updateTimeStatusCode()) {
    matrix.clear();
    drawWord(cw_Es);
    drawWord(cw_Ist);
    drawMinute();
    if (hour() == 0 && timeStatusCode == 0) {
      drawWord(cw_Nacht);
    } else {
      drawHour();
    }
    matrix.writeDisplay();
  }
  delay(5000);
}


//// Time Functions /////

bool updateTimeStatusCode() {
  timeStatusCode = minute() / 5;
  if (timeStatusCode != oldTimeCode) {
#ifdef DEBUG
    printTime();
#endif
    oldTimeCode = timeStatusCode;
    return true;
  } else {
    return false;
  }
}

//// Display Functions /////

void drawMinute() {
  if (timeStatusCode == 1 || timeStatusCode == 5 || timeStatusCode == 7 || timeStatusCode == 11) {
    drawWord(cw_fuenf);
  } else if (timeStatusCode == 2 || timeStatusCode == 8 || timeStatusCode == 10) {
    drawWord(cw_zehn);
  } else if (timeStatusCode == 3) {
    drawWord(cw_viertel);
  } else if (timeStatusCode == 4) {
    drawWord(cw_zwanzig);
  } else if (timeStatusCode == 9) {
    drawWord(cw_dreiviertel);
  }

  if (timeStatusCode >= 5 && timeStatusCode <= 8) {
    drawWord(cw_halb_h);
  }

  if (timeStatusCode >= 1 && timeStatusCode <= 4 || timeStatusCode == 7 || timeStatusCode == 8) {
    drawWord(cw_nach);
  } else if (timeStatusCode ==  5 || timeStatusCode == 10 || timeStatusCode == 11) {
    drawWord(cw_vor);
  }
}

void drawHour() {
  uint8_t h = hourFormat12();
  if (timeStatusCode >= 5) {
    h++;
    if (h > 12) {
      h = 12;
    }
  }
  uint8_t arrPos = h - 1;
  if (timeStatusCode == 0) {
    if (h == 1) {
      drawWord(cw_Ein);
    } else {
      drawWord(cwArr_Hour[arrPos]);
    }
    drawWord(cw_Uhr);
  } else {
    drawWord(cwArr_Hour[arrPos]);
  }
}

void drawWord(ClockWord &w) {
  uint8_t x = (7 - w.row);
  matrix.drawLine(x, w.col, x, w.col + w.width - 1, LED_ON);
}

//// Debug Functions /////

#ifdef DEBUG
void printTime() {
  char sz[32];
  Serial.print(F("Update Time: "));
  sprintf(sz, " %02d:%02d", hour(), minute());
  Serial.print(sz);
  Serial.print(F(" Uhr. timeStatusCode: "));
  Serial.println(timeStatusCode);
}

void printInitStatus() {
  Serial.begin(9600);
  Serial.println(F("16x8 LED Matrix Test - Start RTC"));
  if (timeStatus() != timeSet) {
    Serial.println(F("Unable to sync with the RTC"));
  } else {
    Serial.println(F("RTC has set the system time"));
  }
}
#endif
